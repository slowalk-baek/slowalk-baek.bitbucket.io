// SK 프로보노 메인 페이지용 Javascript

// carousel 설정
var galleryThumbs = new Swiper('.carousel-thumbs', {
  spaceBetween: 1,
  slidesPerView: 4,
  loopedSlides: 4, 
  watchSlidesVisibility: true,
  watchSlidesProgress: true,
});
var galleryTop = new Swiper('.carousel-top', {
  autoplay: {delay: 5000},
  effect: 'fade',
  loop:true,
  loopedSlides: 4, 
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  thumbs: {
    swiper: galleryThumbs,
  },
  pagination: {
    el: '.swiper-pagination',
  },
});

// newsticker 설정
var swiper = new Swiper('.newsticker-container', {
  direction: 'vertical',
  loop: true,
  autoplay: {delay: 5000},
  navigation: {
    nextEl: '.newsticker-button-next',
    prevEl: '.newsticker-button-prev',
  },
});

// 프로보노 스토리 설정
var swiper = new Swiper('.story-container', {
  slidesPerView: 3,
  spaceBetween: 30,
  slidesPerGroup: 3,
  loop: true,
  navigation: {
    nextEl: '.story-button-next',
    prevEl: '.story-button-prev',
  },
  breakpoints: {
    576: {
      slidesPerView: 1,
    },
  },
});