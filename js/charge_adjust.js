    // 담당자 추가
    var countManager = 1;   // 담당자 숫자
    $("#addManager").click(function(){

        countManager++;
        var html = "";

        html+='<table class="table table-type1 table-narrower text-left profile-charge">\n';
        html+='<tbody>\n';
        html+='<tr>\n';
        html+='<th><label for="charge'+countManager+'_name">담당자'+countManager+' 이름<span class="require">*</span></label></th>\n';
        html+='<td><input type="text" id="charge'+countManager+'_name" name="charge'+countManager+'_name"></td>\n';
        html+='</tr>\n';
        html+='<tr>\n';
        html+='<th><label for="charge'+countManager+'_position">담당자'+countManager+' 직급<span class="require">*</span></label></th>\n';
        html+='<td><input type="text" id="charge'+countManager+'_position" name="charge'+countManager+'_position"></td>\n';
        html+='</tr>\n';
        html+='<tr>\n';
        html+='<th>담당자'+countManager+' 연락처<span class="require">*</span></th>\n';
        html+='<td class="input-chainnum">\n';
        html+='<label for="charge'+countManager+'_hp1" class="sr-only">담당자'+countManager+' 전화번호 앞 두 자리에서 세 자리</label>\n';
        html+='<select id="charge'+countManager+'_hp1" required>\n';
        html+='<option id="131" value="02"   >02 </option>\n';
        html+='<option id="132" value="031"  >031</option>\n';
        html+='<option id="133" value="032"  >032</option>\n';
        html+='<option id="134" value="033"  >033</option>\n';
        html+='<option id="135" value="041"  >041</option>\n';
        html+='<option id="136" value="042"  >042</option>\n';
        html+='<option id="137" value="043"  >043</option>\n';
        html+='<option id="138" value="044"  >044</option>\n';
        html+='<option id="139" value="051"  >051</option>\n';
        html+='<option id="140" value="052"  >052</option>\n';
        html+='<option id="141" value="053"  >053</option>\n';
        html+='<option id="142" value="054"  >054</option>\n';
        html+='<option id="143" value="055"  >055</option>\n';
        html+='<option id="144" value="061"  >061</option>\n';
        html+='<option id="145" value="062"  >062</option>\n';
        html+='<option id="146" value="063"  >063</option>\n';
        html+='<option id="147" value="064"  >064</option>\n';
        html+='<option id="148" value="070"  >070</option>\n';
        html+='<option id="149" value="080"  >080</option>\n';
        html+='<option id="122" value="011">011</option>\n';
        html+='<option id="123" value="017">017</option>\n';
        html+='<option id="124" value="016">016</option>\n';
        html+='<option id="125" value="018">018</option>\n';
        html+='<option id="126" value="019">019</option>\n';
        html+='</select>\n';
        html+='<span class="numseparator">-</span>\n';
        html+='<label for="charge'+countManager+'_hp2" class="sr-only">담당자'+countManager+' 전화번호 중간 세자리에서 네자리</label>\n';
        html+='<input type="tel" id="charge'+countManager+'_hp2" minlength="3" maxlength="4" required>\n';
        html+='<span class="numseparator">-</span>\n';
        html+='<label for="charge'+countManager+'_hp3" class="sr-only">담당자'+countManager+' 전화번호 마지막 네자리</label>\n';
        html+='<input type="tel" id="charge'+countManager+'_hp3" minlength="4" maxlength="4" required>\n';
        html+='</td>\n';
        html+='</tr>\n';
        html+='<tr>\n';
        html+='<th><label for="charge'+countManager+'_email_1">담당자'+countManager+' 이메일<span class="require">*</span></label></th>\n';
        html+='<td>\n';
        html+='<div class="form-inline">\n';
        html+='<input type="email" id="charge'+countManager+'_email_1" title="이메일 아이디 입력" required>\n';
        html+='<span class="numseparator">@</span>\n';
        html+='<input type="email" id="charge'+countManager+'_email_2" title="이메일 서비스 제공사 도메인 입력" required>\n'
        html+="<select id=\"charge"+countManager+"_email_3\" name=\"charge"+countManager+"_email_3\"  title=\"이메일 서비스 제공사 도메인 선택\" onChange=\"emailSelBoxUpdated('charge"+countManager+"_email_2', this.value);\">\n";
        html+='<option selected>직접 입력</option>\n';
        html+='<option value="sk.com">sk.com</option>\n';
        html+='<option value="live.co.kr">live.co.kr</option>\n';
        html+='<option value="gmail.com">gmail.com</option>\n';
        html+='<option value="hananet.net">hananet.net</option>\n';
        html+='<option value="hanmail.net">hanmail.net</option>\n';
        html+='<option value="hotmail.com">hotmail.com</option>\n';
        html+='<option value="korea.com">korea.com</option>\n';
        html+='<option value="nate.com">nate.com</option>\n';
        html+='<option value="naver.com">naver.com</option>\n';
        html+='<option value="yahoo.com">yahoo.com</option>\n';
        /*
        html+='<option value="sk.com">sk.com</option>\n';
        html+='<option value="skhynix.com">skhynix.com</option>\n';
        html+='<option value="sktelesys.com">sktelesys.com</option>\n';
        html+='<option value="skcomms.co.kr">skcomms.co.kr</option>\n';
        html+='<option value="happynarae.co.kr">happynarae.co.kr</option>\n';
        html+='<option value="networkons.com">networkons.com</option>\n';
        html+='<option value="skhappiness.org">skhappiness.org</option>\n';
         */
        html+='</select>\n';
        //html+="<span class=\"s_btn gray \" title=\"이메일 인증\" style=\"cursor:pointer;\" onclick=\"certEmail('"+countManager+"')\">이메일 인증</span>\n";
        html+='</div>\n';
        html+='</td>\n';
        html+='</tr>\n';
        html+='</tbody>\n';
        html+='</table>\n';

        //$("section.mg_t50").append(html);
        $(".charge-wrap").append(html);
        if(countManager == 3) {
            $("#addManager").hide();
        }


    });
    // 담당자 삭제
    $(document).on("click", "#delManager", function(){

        if(countManager > 1) {
            $(".charge-wrap").find("table").last().remove();
            countManager--;
            if(countManager < 3) {
                $("#addManager").show();
            }
        } else {
            customAlert("담당자는 최소 1명 이상이여야 합니다.");
        }




    });